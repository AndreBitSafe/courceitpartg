package com.company.task1;

import com.company.task1.data.Generator;
import com.company.task1.model.DataBase;

import java.util.Scanner;

public class RunnerTask1 {
    public void run(){
        DataBase financialDB = new DataBase(Generator.generate());

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your amount in UAH: ");
        int amount = Integer.parseInt(scanner.nextLine());

        System.out.println("Enter type of financial organization (Bank/Exchanger/BlackMarket): ");
        String type = scanner.nextLine().trim();

        financialDB.convertAndPrintInfo(amount, type);

    }
}
