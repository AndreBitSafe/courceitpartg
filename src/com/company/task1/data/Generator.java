package com.company.task1.data;

import com.company.task1.model.Bank;
import com.company.task1.model.BlackMarket;
import com.company.task1.model.Exchanger;
import com.company.task1.model.FinancialOrganization;

public final class Generator {
    private Generator() {
    }

    public static final FinancialOrganization[] generate(){
        FinancialOrganization[] finOrganizations = {
                new Bank("Private", 29.4f),
                new BlackMarket("No name", 30.9f),
                new Bank("Oval", 30.0f),
                new Exchanger("Currency exchange", 30.5f),
                new Bank("Universal", 28.99f),
        };
        return finOrganizations;
    }
}
