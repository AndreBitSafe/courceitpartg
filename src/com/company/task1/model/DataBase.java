package com.company.task1.model;

public class DataBase {
    FinancialOrganization[] financialOrganizations;

    public DataBase(FinancialOrganization[] financialOrganizations) {
        this.financialOrganizations = financialOrganizations;
    }

    public void convertAndPrintInfo(int amount, String type) {
        for (FinancialOrganization financialOrg : financialOrganizations) {
            if (type.equalsIgnoreCase(financialOrg.getType())) {
                System.out.println(financialOrg);
                System.out.printf("\tYour amount: %.2f USD.\n", financialOrg.convert(amount));
            }
        }
    }
}
