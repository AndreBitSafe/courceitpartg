package com.company.task1.model;

public class BlackMarket extends FinancialOrganization {

    private String type = "BlackMarket";
    public String getType() {
        return type;
    }

    public BlackMarket(String name, float usdRate) {
        super(name, usdRate);
    }
}
