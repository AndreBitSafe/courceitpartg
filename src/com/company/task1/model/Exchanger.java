package com.company.task1.model;

public class Exchanger extends FinancialOrganization {

    public static final int MAX_LIM_USD = 20_000;

    private String type = "Exchanger";
    public String getType() {
        return type;
    }

    public Exchanger(String name, float usdRate) {
        super(name, usdRate);
    }

    public float convert (int amount){
        if ((amount / usdRate) < MAX_LIM_USD){
            return super.convert(amount);
        }else {
            System.out.println("Can not sell more then 20 000 USD!!!");
        }
        return 0;
    }
}
