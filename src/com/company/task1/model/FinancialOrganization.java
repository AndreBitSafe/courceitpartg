package com.company.task1.model;

public class FinancialOrganization {

    private String name;
    private String type = "Default";
    protected float usdRate;
    public String getType() {
        return type;
    }

    public FinancialOrganization(String name, float usdRate) {
        this.name = name;
        this.usdRate = usdRate;
    }

    public float convert(int amount){
        return amount/usdRate;
    }

    @Override
    public String toString() {
        return String.format("--> Name: %s, USD rate: %.2f", name, usdRate);
    }
}
