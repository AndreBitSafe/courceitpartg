package com.company.task1.model;

public class Bank extends FinancialOrganization {

    public static final int MAX_LIM_UAH = 150_000;

    private String type = "Bank";
    public String getType() {
        return type;
    }

    public Bank(String name, float usdRate) {
        super(name, usdRate);
    }

    public float convert (int amount){
        if (amount < MAX_LIM_UAH){
            return super.convert(amount);
        }else {
            System.out.println("Can not convert more then 150 000 UAH!!!");
        }
        return 0;
    }
}
