package com.company.task2.model;

public class Yearbook extends Literature {

    private String subjects;
    private String publishingHouse;
    private String type = "Yearbook";

    public String getType() {
        return type;
    }

    public Yearbook(String name, int yearOfPublishing, String subjects, String publishingHouse) {
        super(name, yearOfPublishing);
        this.subjects = subjects;
        this.publishingHouse = publishingHouse;
    }

    @Override
    public String toString() {
        return String.format("%s\tsubjects: %s,%n\tpublishing house: %s.",super.toString(),subjects,publishingHouse);
    }
}
