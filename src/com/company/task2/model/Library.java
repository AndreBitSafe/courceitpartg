package com.company.task2.model;

public class Library {
    Literature[] literature;

    public Library(Literature[] literature) {
        this.literature = literature;
    }

    public void findAndPrintInfo(int inYear, String inType) {
        for (Literature lit : literature) {
            if (lit.isOlder(inYear) && inType.equalsIgnoreCase(lit.getType())) {
                System.out.println(lit);
            }
        }
    }
}
