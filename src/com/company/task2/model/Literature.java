package com.company.task2.model;

public class Literature {
    private String name;
    private String type = "Default";
    private int yearOfPublishing;

    public Literature(String name, int yearOfPublishing) {
        this.name = name;
        this.yearOfPublishing = yearOfPublishing;
    }

    public boolean isOlder(int inYear) {
        return yearOfPublishing < inYear;
    }

    @Override
    public String toString() {
        return String.format("--> Name: %s,%n\tyear of publishing: %d,%n", name,yearOfPublishing);
    }

    public String getType() {
        return type;
    }
}
