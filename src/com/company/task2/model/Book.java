package com.company.task2.model;

public class Book extends Literature {

    private String author;
    private String type = "Book";
    private String publishingHouse;

    public String getType() {
        return type;
    }

    public Book(String name, int yearOfPublishing, String author, String publishingHouse) {
        super(name, yearOfPublishing);
        this.author = author;
        this.publishingHouse = publishingHouse;
    }

    @Override
    public String toString() {
        return String.format("%s\tauthor: %s,%n\tpublishing house: %s.",super.toString(),author,publishingHouse);
    }
}
