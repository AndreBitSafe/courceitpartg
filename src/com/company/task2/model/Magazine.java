package com.company.task2.model;

public class Magazine extends Literature {

    private String subjects;
    private String type = "Magazine";
    private int monthOfPublishing;

    public String getType() {
        return type;
    }

    public Magazine(String name, int yearOfPublishing, String subjects, int monthOfPublishing) {
        super(name, yearOfPublishing);
        this.subjects = subjects;
        this.monthOfPublishing = monthOfPublishing;
    }

    @Override
    public String toString() {
        return String.format("%s\tmonth of publishing: %d,%n\tpublishing house: %s.",super.toString(),monthOfPublishing,subjects);
    }
}
