package com.company.task2;

import com.company.task2.data.LiteratureGenerator;
import com.company.task2.model.Library;

import java.util.Scanner;

public class RunnerTask2 {
    public void run(){
        Library library = new Library(LiteratureGenerator.generate());

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter max year of publishing: ");
        int inYear = Integer.parseInt(scanner.nextLine());

        System.out.println("Enter type of literature (Book/Yearbook/Magazine): ");
        String inType = scanner.nextLine();

        library.findAndPrintInfo(inYear, inType);
    }
}
