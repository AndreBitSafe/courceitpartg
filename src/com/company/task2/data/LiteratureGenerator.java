package com.company.task2.data;

import com.company.task2.model.Book;
import com.company.task2.model.Literature;
import com.company.task2.model.Magazine;
import com.company.task2.model.Yearbook;

public final class LiteratureGenerator {
    private LiteratureGenerator() {
    }
    public static Literature[] generate(){
        Literature[] literature = {
            new Book("Don Juan", 1859,"Jean Batiste Poclene","Book world"),
            new Book("Война и мир", 1873,"Лев Николаевич Толстой","Фиерия"),
            new Magazine("Life stile",2009,"How people live in different countries",12),
            new Yearbook("Земля и люди", 1958,"Змаменитые люди, яркие события в мире","Географизм")
        };
        return literature;
    }
}
